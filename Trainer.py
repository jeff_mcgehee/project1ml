__author__ = 'Jeff'



from sklearn.learning_curve import learning_curve
from sklearn.cross_validation import StratifiedKFold


import numpy as np


import multiprocessing

from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised.trainers import RPropMinusTrainer
import pybrain.datasets as datasets



def crossval_analysis(crossval_data, classifierFun):

    if 'pybrain' in str(type(classifierFun)):

        results = net_crossval(crossval_data)

        return results
    else:

        train_sets = crossval_data[0]
        test_sets = crossval_data[1]

        assert len(train_sets) == len(test_sets), 'There should be an equal number of test and train sets'

        classifiers = []
        for train_data in train_sets:
            classifier = classifierFun

            classifier.fit(train_data.inputs, train_data.targets)
            classifiers.append(classifier)

        test_scores = []
        for ind, classifier, test_data in zip(range(len(classifiers)), classifiers, test_sets):
            test_scores.append(classifier.score(test_data.inputs, test_data.targets))

        # print(test_scores)
        # print(np.mean(test_scores))
        # print(np.std(test_scores))

        return CrossvalResults(classifiers, test_scores)

def net_crossval(crossval_data, net_vars, trainer_vars=None):
        train_sets = crossval_data[0]
        test_sets = crossval_data[1]

        assert len(train_sets) == len(test_sets), 'There should be an equal number of test and train sets'

        num_processors = multiprocessing.cpu_count()

        tasks = multiprocessing.JoinableQueue()
        results = multiprocessing.Queue()

        processors = [multiprocessing.Process(target=_train_single, args=(tasks, results))
                      for i in xrange(num_processors)]
        for processor in processors:
            print('starting processor..')
            processor.start()



        for train_data, test_data in zip(train_sets, test_sets):

            trainset = datasets.ClassificationDataSet(train_data.inputs.shape[1], 1, nb_classes=np.unique(train_data.targets).shape[0])
            for ind, data in enumerate(train_data.inputs):
                trainset.addSample(data, train_data.targets[ind])

            testset = datasets.ClassificationDataSet(test_data.inputs.shape[1], 1 , nb_classes=np.unique(train_data.targets).shape[0])

            for ind, data in enumerate(test_data.inputs):
                testset.addSample(data, test_data.targets[ind])

            trainset._convertToOneOfMany()
            testset._convertToOneOfMany()

            # trainset._convertToOneOfMany()
            tasks.put((trainset, testset, net_vars, trainer_vars))

        for i in xrange(num_processors):
            tasks.put(None)

        tasks.join()

        nets = []
        train_errors = []
        test_errors = []
        for i in range(len(train_sets)):
            out_vals = results.get()
            nets.append(out_vals[0])
            train_errors.append(out_vals[1])
            test_errors.append(out_vals[2])


        return CrossvalResults(nets, test_errors, train_errors)


def _train_single(taskQueue, resultQueue):

    while True:
        task = taskQueue.get()

        if task is None:
            taskQueue.task_done()
            break

        trainset = task[0]
        testset = task[1]
        net_vars = task[2]
        train_vars = task[3]

        if len(net_vars[0]) == 1:
            net = buildNetwork(trainset.indim, net_vars[0][0], trainset.outdim, hiddenclass=net_vars[1], outclass=net_vars[2])
        else:
            net = buildNetwork(trainset.indim, net_vars[0][0], net_vars[0][1], trainset.outdim, hiddenclass=net_vars[1], outclass=net_vars[2])

        trainer = RPropMinusTrainer(net, dataset=trainset, verbose=True)
        print('training...')

        train_errors=[]
        test_errors=[]
        for i in range(50):
            trainer.train()

            if i%2 == 0:
                train_err = 0
                test_err = 0

                for sample, target in zip(trainset['input'],trainset['target']):
                        # print(net.activate(sample),target)
                        if np.argmax(net.activate(sample)) == np.argmax(target):
                            train_err+=0
                        else:
                            train_err+=1

                for sample, target in zip(testset['input'], testset['target']):
                        # print(net.activate(sample),target)
                        if np.argmax(net.activate(sample)) == np.argmax(target):
                            test_err+=0
                        else:
                            test_err+=1


                train_err = 1.-(float(train_err)/float(trainset.getLength()))
                train_errors.append(train_err)

                test_err = 1.-(float(test_err)/float(testset.getLength()))
                test_errors.append(test_err)


        taskQueue.task_done()
        resultQueue.put((net, train_errors, test_errors))

def plot_learning_curve(estimator, axis, inputs, targets, train_color='r', val_color='b', train_name='Train',
                        cross_name='Val.', show_shadows=True, show_train=True):

    train_sizes, train_scores, test_scores = learning_curve(
            estimator, inputs, targets, cv=StratifiedKFold(targets, n_folds=10, shuffle=False), n_jobs=4, train_sizes=np.linspace(.1, 1.0, 5))


    train_sizes = np.linspace(.1, 1.0, 5)

    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)

    ax = axis

    ax.grid()

    if show_shadows:
        if show_train:
            ax.fill_between(train_sizes, train_scores_mean - train_scores_std,
                             train_scores_mean + train_scores_std, alpha=0.1,
                             color=train_color)

        ax.fill_between(train_sizes, test_scores_mean - test_scores_std,
                         test_scores_mean + test_scores_std, alpha=0.1, color=val_color)

    if show_train:
        ax.plot(train_sizes, train_scores_mean, 'o-', linewidth=2, markersize=4, color=train_color,
                 label=train_name)

    ax.plot(train_sizes, test_scores_mean, 'o-', linewidth=2, markersize=4,color=val_color,
             label=cross_name)

    ax.legend(loc="best")

class CrossvalResults(object):
    def __init__(self, predictors, test_scores, train_scores=None):

        self.predictors = predictors
        self.scores = test_scores
        self.train_scores = train_scores

if __name__ == '__main__':
    print('This module is only meant to have imported classes and functions')

