__author__ = 'Jeff'


import FormatData
import Trainer
from AnalyzeData import colorList

from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised.trainers import RPropMinusTrainer
from pybrain.supervised.trainers import BackpropTrainer
import pybrain.datasets as datasets

from pybrain.structure.modules import SoftmaxLayer
from pybrain.structure.modules import SigmoidLayer


import numpy as np
import matplotlib.pyplot as plt

def example_net(num_hidden=5):

    ad_data = FormatData.load_heart()

    print(np.unique(ad_data.targets))

    trainset = datasets.ClassificationDataSet(ad_data.inputs.shape[1], 1, nb_classes=13)

    testset = datasets.ClassificationDataSet(ad_data.inputs.shape[1], 1, nb_classes=13)



    for ind, data in enumerate(ad_data.inputs):
        trainset.addSample(data, ad_data.targets[ind])
        testset.addSample(data, ad_data.targets[ind])

    print(np.unique(trainset['target']))


    trainset._convertToOneOfMany()
    testset._convertToOneOfMany()

    print(trainset.outdim)


    net = buildNetwork(trainset.indim, num_hidden, trainset.outdim, outclass=SoftmaxLayer)

    trainer = RPropMinusTrainer(net, dataset=trainset, deltamax=1.0, verbose=True)

    train_errors = []
    for i in range(50):
        trainer.train()
        error = 0
        for sample, target in zip(trainset['input'],trainset['target']):
                # print(net.activate(sample),target)
                if np.argmax(net.activate(sample)) == np.argmax(target):
                    error+=0
                else:
                    error+=1
        error = 1.-float(error)/float(trainset.getLength())
        train_errors.append(error)


    fig = plt.figure()
    ax = fig.add_subplot(111)

    ax.plot(train_errors)

    plt.show()

    # trainer.trainUntilConvergence()

    print('Test Results:')
    print(trainer.testOnData(dataset=testset))


def net_analysis():

    datasets = [FormatData.load_heart(),  FormatData.loadAd()]

    for data in datasets:
        # print(data.metadata['name'])
        fig = plt.figure()
        ax = fig.add_subplot(111)

        for hidden in [250]:

            results = Trainer.net_crossval(data.kfold(10),((hidden,), SigmoidLayer, SoftmaxLayer))


            print(results.scores)
            print(results.train_scores)

            scores = np.mean(np.asarray(results.scores), axis=0)
            print(scores.shape)
            train_scores = np.mean(np.asarray(results.train_scores), axis=0)
            print(train_scores.shape)

            # for score, train in zip(results.scores, results.train_scores):
            #     ax.plot(score)
            #     ax.plot(train)
            ax.plot(range(2, 52, 2), train_scores, linewidth=2, color=colorList[0], label=str(hidden)+': Training')
            ax.plot(range(2, 52, 2), scores, linewidth=2, color=colorList[1], label=str(hidden)+': Validation')

        ax.set_xlabel('Training Epochs')
        ax.set_ylabel('Accuracy (%)')
        ax.set_title(data.metadata['name'] + ' Neural Network Training')
        ax.grid()
        ax.legend(loc='best')

        # print(results.scores)

    plt.show()


if __name__ == '__main__':
    net_analysis()


