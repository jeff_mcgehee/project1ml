__author__ = 'Jeff'

import numpy as np
import csv
import os

from sklearn.cross_validation import KFold
from sklearn import preprocessing
from sklearn.feature_selection import chi2

from sklearn.decomposition import PCA
from sklearn.feature_selection import SelectKBest

#Read in .csv or something into pybrain dataset.
#Format it to work with sklearn stuff too.

import sklearn.tree as tree

def loadAd(noNAN=True):
    with open('./ad-dataset/ad.names','rb') as read_file:
        reader = csv.reader(read_file, delimiter=':')
        namedata = []
        for row in reader:

            if len(row) < 1:
                continue

            elif '|' in row[0]:
                if 'classes' in row[0]:
                    initsplit = row[0].split('|')
                    finalsplit = initsplit[0].split(',')

                    classes = finalsplit+[initsplit[-1]]

                continue

            namedata.append(row[0])

    metadata = {'input_names':namedata}
    metadata['target_names'] = classes

    with open('./ad-dataset/ad.data','rb') as read_file:
        reader = csv.reader(read_file)

        data = []
        for row in reader:
            if row[-1] == 'ad.':
                row[-1] = 1.
            else:
                row[-1] = 0.

            for ind,item in enumerate(row):
                try:
                    row[ind] = float(item)

                except ValueError:
                    row[ind] = 'nan'
            data.append(row)

        data = np.asarray(data,dtype='float')

    if noNAN:
        data = data[~np.isnan(data).any(axis=1)]
    else:
        imp = preprocessing.Imputer()

        data = imp.fit_transform(data)

    np.random.shuffle(data)

    input = data[:,:-1]
    target = data[:,-1]

    metadata['name'] = 'Ads'

    # print(input.shape)
    return DataSet(input,target,metadata)

def load_religion(important=False):

    with open('./religion-data/atusreligion.csv','rU') as read_file:
        reader = csv.reader(read_file)

        data = []
        for ind, row in enumerate(reader):
            if ind == 0:
                metadata = {'input_names':row[:-1]}
                metadata['target_names'] = row[-1]
            else:
                data.append(row)

    data = np.asarray(data, dtype='float')

    input = data[:,:-1]
    if important:
        input = input[:, [0, 5, 6]]

    target = data[:,-1]

    scaler = preprocessing.StandardScaler().fit(input)
    input = scaler.transform(input)




    metadata['data_scaler'] = scaler

    return DataSet(input, target, metadata)

def load_pen():
    datadir = './pen-data'

    allwriters = []
    for name in os.listdir(datadir):

        with open(os.path.join(datadir,name)) as datafile:

            reader = csv.reader(datafile)

            writer_data = []

            keep_flag = False
            for row in reader:

                if len(row) > 0:
                    if '.SEGMENT CHARACTER' in row[0]:
                        keep_flag = True
                        writer_data.append([])
                    elif '.DT 100' in row[0]:
                        keep_flag = False
                    elif keep_flag:
                        writer_data[-1].append(row)
        allwriters.append(writer_data)


    restruct = []
    for i in range(124): #124 characters written by each writer
        restruct.append([])
        for writer in allwriters:
            restruct[-1].append(writer[i]+[[str(i)]])

    restruct1 = []
    for ind, a_letter in enumerate(restruct):

        letter_length = max([len(writer) for writer in a_letter])

        for writer in a_letter:
            while len(writer) < letter_length:
                writer.append([' 0 0'])

            restruct1.append([])
            class_ind = 0
            for val in writer:
                # print(val)

                val = val[0].split(' ')

                if val[0] in ('.PEN_UP', '.PEN_DOWN'):
                    continue

                if any(['Class' in item for item in val]):

                    restruct1[-1].append(val[2])
                    # restruct1[-1].append(str(ind))

                elif any([item.isdigit() for item in val]):
                    for item in val:
                        if item.isdigit():
                            restruct1[-1].append(item)

    cipher_dict = {'[A]': 0,
                   '[B]': 1,
                   '[C]': 2,
                   '[D]': 3,
                   '[E]': 4,
                   '[F]': 5,
                   '[G]': 6,
                   '[H]': 7,
                   '[I]': 8,
                   '[J]': 9,
                   '[K]': 10,
                   '[L]': 11,
                   '[M]': 12,
                   '[N]': 13,
                   '[O]': 14,
                   '[P]': 15,
                   '[Q]': 16,
                   '[R]': 17,
                   '[S]': 18,
                   '[T]': 19,
                   '[U]': 20,
                   '[V]': 21,
                   '[W]': 22,
                   '[X]': 23,
                   '[Y]': 24,
                   '[Z]': 25,
                   '[1]': 26,
                   '[2]': 27,
                   '[3]': 28,
                   '[4]': 29,
                   '[5]': 30,
                   '[6]': 31,
                   '[7]': 32,
                   '[8]': 33,
                   '[9]': 34}

    letter_length = max([len(letter) for letter in restruct1])
    for letter in restruct1:
        while len(letter) < letter_length:
            letter.append('0')
        letter[0] = cipher_dict[letter[0]]

    final_struct = np.asarray(restruct1, dtype='int')
    # print(final_struct.shape)

    inputs = final_struct[:,1:]
    targets = final_struct[:,0]

    # print(inputs.shape)

    scaler = preprocessing.StandardScaler().fit(inputs)
    inputs = scaler.transform(inputs)

    # pca = PCA(n_components=100)
    # # inputs = pca.fit_transform(inputs)
    # inputs = SelectKBest(chi2, k=50).fit_transform(inputs, targets)

    # print(inputs.shape)
    # print(targets)

    metadata = {'input_names': 'pen_locations'}
    metadata['target_names'] = cipher_dict
    metadata['data_scaler'] = scaler

    return DataSet(inputs, targets, metadata)



def load_parkinsons():

    with open('./parkinsons-data/parkinsons.data', 'rU') as datafile:

        reader = csv.reader(datafile)

        row_dict = {}
        data = []
        for ind, row in enumerate(reader):
            if ind == 0:
                for item_ind, item in enumerate(row):
                    row_dict[item] = item_ind
            else:
                row_data = []
                for item_ind, item in enumerate(row):
                    if row_dict['status'] == item_ind or item_ind==0:
                        continue
                    else:
                        row_data.append(item)
                row_data.append(row[row_dict['status']])

                data.append(row_data)

    data = np.asarray(data, dtype='float')

    inputs = data[:,:-1]

    targets = data[:,-1].astype(int)

    # print(targets)

    scaler = preprocessing.StandardScaler().fit(inputs)
    inputs = scaler.transform(inputs)

    metadata = {'input_names': row_dict}
    metadata['target_names'] = '[1, 0] has parkinsons'
    metadata['data_scaler'] = scaler

    return DataSet(inputs, targets, metadata)




def load_bc():
    with open('./contraceptive-data/cmc.data', 'rU') as datafile:

        reader = csv.reader(datafile)

        row_dict = {}
        data = []
        for ind, row in enumerate(reader):
            # if ind == 0:
            #     for item_ind, item in enumerate(row):
            #         row_dict[item] = item_ind
            # else:
            #     row_data = []
            #     for item_ind, item in enumerate(row):
            #         if row_dict['status'] == item_ind or item_ind==0:
            #             continue
            #         else:
            #             row_data.append(item)
            #     row_data.append(row[row_dict['status']])
            #
            #     data.append(row_data)
            data.append(row)




    data = np.asarray(data, dtype='float')

    inputs = data[:,:-1]

    targets = data[:,-1].astype(int)

    # print(targets)

    scaler = preprocessing.StandardScaler().fit(inputs)
    inputs = scaler.transform(inputs)

    metadata = {'input_names': row_dict}
    metadata['target_names'] = '[1, 0] has parkinsons'
    metadata['data_scaler'] = scaler

    return DataSet(inputs, targets, metadata)

def load_heart():
    with open('./arrhythmia/arrhythmia.data', 'rU') as datafile:

        reader = csv.reader(datafile)

        row_dict = {}
        data = []
        for ind, row in enumerate(reader):
            float_data = []
            for item in row:
                try:
                    float_data.append(float(item))
                except ValueError:
                    float_data.append('NaN')
            data.append(float_data)


    imp = preprocessing.Imputer()

    data = imp.fit_transform(data)

    data = np.asarray(data, dtype='float')

    inputs = data[:,:-1]

    targets = data[:,-1].astype(int)

    for ind, target in enumerate(targets):
        if target == 14:
            targets[ind] = 11
        if target == 15:
            targets[ind] = 12
        if target == 16:
            targets[ind] = 13

    targets = targets-1

    # print(targets)

    scaler = preprocessing.StandardScaler().fit(inputs)
    inputs = scaler.transform(inputs)


    names = [
            'Age',
            'Sex',
            'Height',
            'Weight',
            'QRS duration',
            'P-R interval',
            'Q-T interval',
            'T interval',
            'P interval',
            'QRS',
            'T',
            'P',
            'QRST',
            'J',
            'Heart rate',
            'Q wave',
            'R wave',
            'S wave',
            'R wave',
            'S wave',
            'Number of intrinsic deflections',
            'Existence of ragged R wave',
            'Existence of diphasic derivation of R wave',
            'Existence of ragged P wave',
            'Existence of diphasic derivation of P wave',
            'Existence of ragged T wave',
            'Existence of diphasic derivation of T wave'
            ] + [str(val) for val in range(28,280)]

    metadata = {'input_names': names}

    metadata['target_names'] = ['Normal',
                                'Coronary Artery Disease',
                                'Old Anterior Myocardial Infarc.',
                                'Old Inferior Myocardial Infarc.',
                                'Sinus tachycardy',
                                'Sinus bradycardy',
                                'Ventricular Pre. Contract.',
                                'Supraventricular Pre. Contract.',
                                'Left bundle branch block',
                                'Right bundle branch block',
                                # '1st degree AtrioVentricular block',
                                # '2nd degree AV block',
                                # '3rd degree AV block',
                                'Left ventricule hypertrophy',
                                'Atrial Fibrillation or Flutter',
                                'Other'
                                ]

    metadata['data_scaler'] = scaler
    metadata['name'] = 'Arrhythmia'

    print(inputs.shape)

    return DataSet(inputs, targets, metadata)


class DataSet(object):
    def __init__(self, inputs, targets, metadata, importances=None):


        assert 'numpy.ndarray' in str(type(inputs)) and 'numpy.ndarray' in str(type(targets)), \
            "inputs and targets must be type numpy.ndarray"

        assert inputs.shape[0] == targets.shape[0], "must have same number of inputs and targets"

        assert type(metadata) is dict, "metadata must be type dict"

        req_keys = ['input_names','target_names']

        assert all([key in metadata.keys() for key in req_keys]), \
            "metadata must have keys 'input_names and 'target_names'"


        self.inputs = inputs
        self.targets = targets

        self.metadata = metadata

        self.importances = importances

    @property
    def num_samps(self):
        return self.inputs.shape[0]

    @property
    def indim(self):
        try:
            return self.inputs.shape[1]
        except IndexError:
            return 1

    @property
    def outdim(self):
        try:
            return self.targets.shape[1]
        except IndexError:
            return 1

    def kfold(self, k):


        kf = KFold(self.inputs.shape[0], n_folds=k, shuffle=True)

        train_sets = []
        test_sets = []

        for train_ind, test_ind in kf:

            inputs_train, inputs_test = self.inputs[train_ind], self.inputs[test_ind]
            targets_train, targets_test = self.targets[train_ind], self.targets[test_ind]

            # train_meta = {'input_names': [self.metadata['input_names'][ind] for ind in train_ind]}
            # train_meta['target_names'] = self.metadata['target_names']
            #
            # test_meta = {'input_names': [self.metadata['input_names'][ind] for ind in test_ind]}
            # test_meta['target_names'] = self.metadata['target_names']

            test_sets.append(DataSet(inputs_test, targets_test, self.metadata))
            train_sets.append(DataSet(inputs_train, targets_train, self.metadata))

        return train_sets, test_sets

    def reduced_inputs(self, threshold=None):

        clf = tree.DecisionTreeClassifier()

        clf.fit(self.inputs, self.targets)

        importances = clf.feature_importances_

        keepInds = np.asarray([ind for ind, val in enumerate(importances) if val > threshold])

        vals = self.inputs[:, keepInds]

        new_meta = self.metadata

        new_meta['input_names'] = [new_meta['input_names'][ind] for ind in keepInds]

        return DataSet(vals, self.targets, new_meta, importances[keepInds])

    def random_select(self,percentage=0.1):

        data = np.append(self.inputs, np.asarray([self.targets]).T, axis=1)

        np.random.shuffle(data)

        numInds = np.rint(data.shape[0]*percentage)

        return DataSet(data[:numInds,:-1],data[:numInds,-1],self.metadata)

if __name__ == '__main__':
    ad_data = loadAd()
    #
    # train_sets, test_sets = ad_data.kfold(10)
    #
    # print(len(train_sets))
    # print(len(test_sets))
    #
    # for set in train_sets:
    #     print(set[0].shape)
    #
    # for set in test_sets:
    #     print(set[0].shape)

    # load_pen()

    # load_parkinsons()

    # bc = load_bc()

    # bc.random_select()

