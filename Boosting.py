__author__ = 'Jeff'

import FormatData
import Trainer
from AnalyzeData import colorList

from sklearn.ensemble import AdaBoostClassifier

import sklearn.tree as tree

import matplotlib.pyplot as plt



def adaboost_analysis():

    datasets = [FormatData.load_heart(), FormatData.loadAd()]

    for data in datasets:
        # booster = GradientBoostingClassifier()
        booster50 = AdaBoostClassifier(base_estimator=tree.DecisionTreeClassifier(min_samples_split=30), n_estimators=50)

        booster25 = AdaBoostClassifier(base_estimator=tree.DecisionTreeClassifier(min_samples_split=30), n_estimators=25)
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.grid()
        ax.set_title(data.metadata['name']+' Adaboost Learning Curves')
        ax.set_ylabel('Accuracy (%)')
        # ax.set_ylim(0.95,1.05)
        ax.set_xlabel('Number of Samples (% of full set)')


        Trainer.plot_learning_curve(booster25,ax,data.inputs,data.targets, show_shadows=False,
                                    train_name='25 Est. Train', cross_name='25 Est. Val',
                                    train_color=colorList[0], val_color=colorList[6])

        Trainer.plot_learning_curve(booster50,ax,data.inputs,data.targets, show_shadows=False,
                                    train_name='50 Est. Train', cross_name='50 Est. Val',
                                    train_color=colorList[7], val_color=colorList[8])

    print('Plotting...')
    plt.show()




if __name__ == '__main__':
    adaboost_analysis()