__author__ = 'Jeff'


import FormatData
import Trainer

import sklearn.tree as tree

import numpy as np


import matplotlib.pyplot as plt

from AnalyzeData import colorList




def dtree_analysis():

    datasets = [FormatData.load_heart(), FormatData.loadAd()]


    figure = plt.figure()
    ax = figure.add_subplot(111)
    ax.grid()
    ax.set_title('Cross Validation Accuracy of Pruned and Unpruned Trees')
    ax.set_ylabel('Accuracy (%)')
    ax.set_xlabel('Number of Samples (% of full set)')

    for ind, data in enumerate(datasets):

        unpruned = tree.DecisionTreeClassifier()

        unpruned.fit(data.inputs, data.targets)

        # print(unpruned.feature_importances_)

        new_inputs = unpruned.transform(data.inputs)


        Trainer.plot_learning_curve(tree.DecisionTreeClassifier(),ax, data.inputs, data.targets,val_color=colorList[0+ind],
                                    cross_name=data.metadata['name']+'Unpruned', show_train=False, show_shadows=False)

        Trainer.plot_learning_curve(tree.DecisionTreeClassifier(min_samples_split=10),ax, new_inputs, data.targets,
                                    val_color=colorList[5+ind], cross_name=data.metadata['name']+'Pruned', show_train=False,
                                    show_shadows=False)




        unpruned_results = Trainer.crossval_analysis(data.kfold(10), tree.DecisionTreeClassifier())
        # print(unpruned_results.scores)
        unpruned_sizes = [dtree.tree_.node_count for dtree in unpruned_results.predictors]
        print('Unpruned Number of Nodes: %0.2f'%np.mean(unpruned_sizes))

        data.inputs = new_inputs
        pruned_results = Trainer.crossval_analysis(data.kfold(10), tree.DecisionTreeClassifier(min_samples_split=10))
        pruned_sizes = [dtree.tree_.node_count for dtree in pruned_results.predictors]
        print('Pruned Number of Nodes: %0.2f'%np.mean(pruned_sizes))
        # print(pruned_results.scores)



    plt.show()





if __name__ == '__main__':
    dtree_analysis