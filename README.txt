Jeff McGehee CSC 7641 Project 1

* All code is written in Python
* All data relevant to my analysis is in /ad-dataset and /arrhythmia.  The code will run with the other data sets, but
    should not be expected to do so.  I had trouble picking data for my problems, so I ended up downloading extra sets.
* Package requirements are numpy, scipy, matplotlib, scikitlearn and pybrain.
* To generate results found in jmcgehee3-analysis.pdf in the order they are presented in the paper
    - Run AnalyzeData.py
    - Run DecisionTree.py
    - Run Boosting.py
    - Run kNN.py
    - Run SupportVectorMachines.py
    - Run NeuralNetwork.py

* FormatData.py and Trainer.py contain functions and classes called by the main algorithm files.