__author__ = 'Jeff'

from sklearn.svm import SVC

import FormatData
import Trainer
from AnalyzeData import colorList

import matplotlib.pyplot as plt




def analyze_svm():
    datasets = [FormatData.loadAd(), FormatData.load_heart()]

    for data in datasets:
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.grid()
        ax.set_title(data.metadata['name']+' SVM Learning Curves')
        ax.set_ylabel('Accuracy (%)')
        ax.set_xlabel('Number of Samples (% of full set)')

        for ind, k in enumerate(['sigmoid', 'linear', 'poly', 'rbf']):

            neighbors = SVC(kernel=k)

            Trainer.plot_learning_curve(neighbors, ax, data.inputs, data.targets, show_shadows=False, show_train=True,
                                        cross_name=str(k)+' Val.', val_color=colorList[0+ind],
                                        train_name=str(k)+' Train', train_color=colorList[6+ind])

    plt.show()
if __name__ == '__main__':
    analyze_svm()