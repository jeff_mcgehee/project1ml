__author__ = 'Jeff'


import FormatData
import Trainer
from AnalyzeData import colorList

from sklearn.neighbors import KNeighborsClassifier

import matplotlib.pyplot as plt


def knn_analysis():

    datasets = [FormatData.load_heart(), FormatData.loadAd()]

    for data in datasets:
        fig = plt.figure()
        ax = fig.add_subplot(111)

        ax.grid()
        ax.set_title(data.metadata['name']+' Cross Validation kNN Learning Curves')
        ax.set_ylabel('Accuracy (%)')
        ax.set_xlabel('Number of Samples (% of full set)')

        for ind, k in enumerate([1, 5, 10, 15]):

            neighbors = KNeighborsClassifier(k, weights='distance')

            Trainer.plot_learning_curve(neighbors, ax, data.inputs, data.targets, show_shadows=False, show_train=False,
                                        cross_name=str(k)+'Neigh.', val_color=colorList[3*ind])

    print('Plotting...')
    plt.show()


if __name__ == '__main__':
    knn_analysis()

